package practicos.practico1;
import java.util.Scanner;
public class ejercicio5 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese el valor del radio de la una circunferencia");
        float r = s.nextFloat();
        double area = Math.PI*(r*r);
        double perimeter= Math.PI*2*r;
        System.out.println("El valor del area es de: "+area);
        System.out.println("El valor del perimetro es de: "+perimeter);
    }
}
