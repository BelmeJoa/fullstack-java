package practicos.practico1;
import java.util.Scanner;
public class ejercicio9 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in); 
        System.out.println("Ingrese el valor en pesos");
        float pesos = s.nextFloat();
        System.out.printf("%n %.2f pesos : ",pesos);
        System.out.printf("%n Equivale a %.2f dolares.",pesos/231.68);
        System.out.printf("%n Equivale a %.2f euros.",pesos/250.69);
        System.out.printf("%n Equivale a %.2f guaranies.",pesos*31);
        System.out.printf("%n Equivale a %.2f reales.",pesos/46.81);
    }
}
