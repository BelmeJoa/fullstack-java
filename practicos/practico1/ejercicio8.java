package practicos.practico1;
import java.util.Scanner;
public class ejercicio8 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese el valor de la temperatura en grados Celcius");
        float temp = s.nextFloat();
        System.out.printf("%n El valor de la temperatura en grados Kelvin es: %.2f",(temp+273.15));
        System.out.printf("%n El valor de la temperatura en grados Fahrenheit es : %.2f",((temp*1.8)+32));            
    }
    
}
